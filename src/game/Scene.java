package game;

/**
 *
 * @author Jakub
 */
public class Scene {
    
    private int width;
    private int height;
    private String[][] terrain;
    private String[][] terrainObjects;
    
    /**
     * Create Scene object
     * @param height - int - indicates vertical size
     * @param width - int - indicates horozontal size
     */
    public Scene (int width, int height) {
        this.width = width;
        this.height = height;
        terrain = new String[width][height];
        terrainObjects = new String[width][height];
        
    }
    
    // Mutator methods
    
    /**
     * Specified type of terrain at given location in the scene
     * @param terrain - description of the type of terrain
     * @param row - horizontal coordinate
     * @param column - vertical coordinate
     */
    public void addTerrain(String terrain, int row, int column) {
        this.terrain[row][column] = terrain;
    }
    /**
     * Add object at given location
     * @param terrainObject specify object as string
     * @param row horizontal coordinate
     * @param column vertical coordinate
     */
    public void addTerrainObjects(String terrainObject, int row, int column) {
        terrainObjects[row][column] = terrainObject;
    }
    
    // Accessor methods
    
    public int getHeight() {
        return height;
    }
    
    public int getWidth() {
        return width;
    }
    
    public String getTerrain(int row, int column) {
        return terrain[row][column];
    }
    
    public String getTerrainObject(int row, int column) {
        return terrainObjects[row][column];
    }
    
    /**
     * measure distance between two objects on the scene
     * 
     * @param hor_1 - int horizontal coordinate of first object
     * @param vert_1 - int vertical coordinate of first object
     * @param hor_2 - int horizontal coordinate of second object
     * @param vert_2 - int vertical coordinate of second object
     * @return int - distance between two objects
     */
    public int distance(int hor_1, int vert_1, int hor_2, int vert_2) {
        int distance = Math.abs(hor_2 - hor_1) + Math.abs(vert_2 - vert_1);
        return distance;
    }
    
    /**
     * check if a given cell is empty
     * @param row - int object row
     * @param column - int object column
     */
    public boolean isEmpty(int row, int column) {
        return terrain[row][column] == null;
    }
    
    /**
     * count the cells that are not empty
    */
    public int countItems(int row, int column, int maxDistance) {
        int count = 0;
        // in my version of the program I traverse the scene array
        // horizontally first and then vertically
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(!isEmpty(i, j)) {
                    // test if the current point is within the maximum distance
                    // from the (0,0) coordinate
                    if((distance(row, column, i, j) <= maxDistance)) {
                        System.out.println("Object found: " + terrain[i][j]);
                        count +=1;
                    }
                }
            }
        }
        return count;
    }
    
}
