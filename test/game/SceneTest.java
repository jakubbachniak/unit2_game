
package game;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jakub
 */

public class SceneTest {

    /**
     * Test of getHeight method, of class Scene.
     */
    @Test
    public void testGetHeight() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getHeight());
        Scene scene2 = new Scene(10, 5);
        assertEquals(5, scene2.getHeight());
    }
    @Test
    public void testGetWeight() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getWidth());
        Scene scene2 = new Scene(12, 6);
        assertEquals(12, scene2.getWidth());
    }
    @Test
    public void testAddTerrain() {
        Scene scene = new Scene(5, 3);
        scene.addTerrain("G", 2, 1);
        scene.addTerrain("T", 4, 2);
        assertEquals("G", scene.getTerrain(2, 1));
        assertEquals("T", scene.getTerrain(4, 2));
    }
    @Test
    public void testAddTerrainObjects() {
        Scene scene = new Scene(6, 8);
        scene.addTerrainObjects("Stone", 2, 4);
        scene.addTerrainObjects("Stick", 3, 5);
        assertEquals("Stone", scene.getTerrainObject(2, 4));
        assertEquals("Stick", scene.getTerrainObject(3, 5));
    }
    @Test
    public void testDistance() {
        Scene scene = new Scene(4, 2);
        assertEquals(1, scene.distance(2, 0, 1, 0));
        assertEquals(2, scene.distance(1, 0, 1, 2));
        assertEquals(5, scene.distance(4, 2, 1, 0));
    }
    @Test
    public void testIsEmpty () {
        Scene scene = new Scene(5, 3);
        scene.addTerrain("Stones", 1, 1);
        scene.addTerrain("Trees", 3, 2);
        assertFalse(scene.isEmpty(1, 1));
        assertFalse(scene.isEmpty(3, 2));
    }
    @Test
    public void testCountItems() {
        Scene scene = new Scene(5, 3);
        scene.addTerrain("Stone", 1, 1);
        scene.addTerrain("Tree", 3, 2);
        assertEquals(2, scene.countItems(0, 0, 5));
    }
}